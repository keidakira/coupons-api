package in.mavex.mavexcouponsapi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CouponsChild {

    private @Id @GeneratedValue Long id;
    private @Column String couponCode;
    private @Column String eventId;
    private @Column String ticketType;

    public CouponsChild() {}

    public CouponsChild(String couponCode, String eventId, String ticketType) {
        this.couponCode = couponCode;
        this.eventId = eventId;
        this.ticketType = ticketType;
    }

}
