package in.mavex.mavexcouponsapi;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;

public interface CouponsRepository extends JpaRepository<Coupons, Long> {
    List<Coupons> findByCouponCode(String couponCode);
}
