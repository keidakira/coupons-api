package in.mavex.mavexcouponsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MavexCouponsApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MavexCouponsApiApplication.class, args);
    }

}
