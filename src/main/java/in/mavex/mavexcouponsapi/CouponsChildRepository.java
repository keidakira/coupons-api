package in.mavex.mavexcouponsapi;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;

public interface CouponsChildRepository extends CrudRepository<CouponsChild, Long> {

    List<CouponsChild> findByEventId(String eventId);

}
