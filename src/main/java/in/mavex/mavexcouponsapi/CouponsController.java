package in.mavex.mavexcouponsapi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.sql.Array;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path="/coupons")
public class CouponsController {
    @Autowired
    private CouponsRepository couponsRepository;
    private CouponsChildRepository couponsChildRepository;
    @PostMapping(path="/create")
    public @ResponseBody
    JsonObject createCoupon(@RequestParam String couponCode, @RequestParam String couponName,
                        @RequestParam String couponType, @RequestParam String description,
                        @RequestParam double percentage, @RequestParam int priceLimit,
                        @RequestParam int flatPrice, @RequestParam String validFrom,
                        @RequestParam String validUpto, @RequestParam String eventId, @RequestParam String ticketType) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("status", "400");

        System.out.println(couponsRepository.findByCouponCode(couponCode).size());
        if(couponsRepository.findByCouponCode(couponCode).size() != 0) {
            // Return error message
            jsonObject.addProperty("data", "couponCode already exists!");
            return jsonObject;
        }

        if(couponCode.isEmpty()) {
            jsonObject.addProperty("data", "couponCode cannot be empty!");
            return jsonObject;
        }

        if(couponName.isEmpty()) {
            jsonObject.addProperty("data", "couponName cannot be empty!");
            return jsonObject;
        }

        if(couponType.isEmpty()) {
            jsonObject.addProperty("data", "type cannot be empty!");
            return jsonObject;
        }

        if(description.isEmpty()) {
            jsonObject.addProperty("data", "Description cannot be empty!");
            return jsonObject;
        }

        if(couponType.equals("Discount") && (percentage <= 0)) {
            jsonObject.addProperty("data", "Invalid value for percentage!");
            return jsonObject;
        }

        if(couponType.equals("Discount") && (priceLimit <= 0)) {
            jsonObject.addProperty("data", "Invalid value for priceLimit!");
            return jsonObject;
        }

        if(couponType.equals("Flat") && (flatPrice <= 0)) {
            jsonObject.addProperty("data", "Invalid value for flatPrice!");
            return jsonObject;
        }

        // Checking for valid dates
        Timestamp todayDate = new Timestamp(System.currentTimeMillis());
        Timestamp timeValidFrom = Timestamp.valueOf(validFrom);
        Timestamp timeValidUpto = Timestamp.valueOf(validUpto);

        if(timeValidFrom.compareTo(todayDate) < 0 || timeValidFrom.compareTo(timeValidUpto) > 0) {
            // Invalid dates
            jsonObject.addProperty("data", "Invalid dates provided!");
            return jsonObject;
        }

        // Add a row in coupons table
        Coupons coupons = new Coupons(couponCode, couponName, couponType, description, percentage, priceLimit, flatPrice, timeValidFrom, timeValidUpto);
        couponsRepository.save(coupons);

        // Add a row in couponsChild table
        String[] ticketList = ticketType.split(",");
        for(String ticket: ticketList) {
            CouponsChild couponsChild = new CouponsChild(couponCode, eventId, ticket);
            couponsChildRepository.save(couponsChild);
        }
        Gson gsonBuilder = new GsonBuilder().create();
        JsonObject response = new JsonObject();
        response.addProperty("couponCode", couponCode);
        response.addProperty("couponName", couponName);
        response.addProperty("type", ticketType);
        response.addProperty("description", description);
        response.addProperty("percentage", percentage);
        response.addProperty("priceLimit", priceLimit);
        response.addProperty("flatPrice", flatPrice);
        response.addProperty("ValidFrom", validFrom);
        response.addProperty("validUpto", validUpto);
        response.addProperty("eventId", eventId);
        response.addProperty("ticketType", ticketType);
        return response;
    }

    @GetMapping(path = "/get", params = "couponCode")
    public @ResponseBody
    String getCouponById(@RequestParam String couponCode) {
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        List<Coupons> couponsList = couponsRepository.findByCouponCode(couponCode);
        if(couponsList.size() == 0) {
            jsonObject.addProperty("status", "400");
            jsonObject.addProperty("data", "Given couponCode does not exist!");
            return jsonObject.toString();
        } else {
            return gson.toJson(couponsList);
        }
    }

    @GetMapping(path = "/get", params = "eventId")
    public @ResponseBody
    String getCouponsByEventId(@RequestParam String eventId) {
        Gson gson = new Gson();
        List<Coupons> eventCoupons = couponsRepository.getCouponsByEventId(eventId);

        return gson.toJson(eventCoupons);
    }

}
