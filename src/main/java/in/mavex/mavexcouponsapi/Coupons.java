package in.mavex.mavexcouponsapi;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
class Coupons {
    private @Id @GeneratedValue Long couponId;
    private @Column String couponCode;
    private @Column String couponName;
    private @Column String type;
    private @Column String description;
    private @Column double percentage;
    private @Column int priceLimit;
    private @Column int flatPrice;
    private @Column Timestamp validFrom;
    private @Column Timestamp validUpto;
    @ManyToMany(cascade = CascadeType.ALL)
    private List<CouponsChild> couponsChild;
    public Coupons() {}

    Coupons(String couponCode, String couponName, String type, String description, double percentage, int priceLimit, int flatPrice, Timestamp validFrom, Timestamp validUpto) {
        this.couponCode = couponCode;
        this.couponName = couponName;
        this.type = type;
        this.description = description;
        this.percentage = percentage;
        this.priceLimit = priceLimit;
        this.flatPrice = flatPrice;
        this.validFrom = validFrom;
        this.validUpto = validUpto;
    }

}